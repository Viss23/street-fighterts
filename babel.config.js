const presets = [
  [
    "@babel/preset-typescript",
    {
      targets: {
        firefox: "60",
        chrome: "67"
      }
    }
  ]
];

const plugins = [ "@babel/plugin-proposal-class-properties",
                "@babel/proposal-object-rest-spread" ,
                "@babel/plugin-transform-typescript"];
  
module.exports = { 
  presets, 
  plugins 
};