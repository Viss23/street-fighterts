type Attr = { [key: string]: string };

type IElement = {
  tagName: string,
  className?: string,
  attributes?: Attr
} 


/* ({ tagName, className, attributes = {} }) */
export function createElement({ tagName, className, attributes = {} }: IElement) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
