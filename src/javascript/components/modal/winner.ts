import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { IFighterDetails} from '../../../interfaces/types';

export function showWinnerModal(fighter:IFighterDetails) {
  const bodyElement = createElement({
    tagName: "div",
    className: "fighterWinBody"
  })

  const span = createElement({
    tagName: "span",
    className: "fighterText"
  })

  span.innerHTML= `${fighter.name} WINS`;

  bodyElement.append(span);

  showModal({title:'K.O.',bodyElement});

}
