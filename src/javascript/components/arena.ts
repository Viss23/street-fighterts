import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import {showWinnerModal} from './modal/winner';
import { IFighterDetails} from '../../interfaces/types';

export function renderArena(selectedFighters: IFighterDetails[]) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root!.innerHTML = '';
  root!.append(arena);

  const [firstFighter,secondFighter] = selectedFighters

   fight(firstFighter,secondFighter).then( (winner) =>{
    showWinnerModal(winner)
  })

}

function createArena(selectedFighters: IFighterDetails[]) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const [firstFighter, secondFighter] = selectedFighters
  const healthIndicators = createHealthIndicators(firstFighter,secondFighter);
  const fighters = createFighters(firstFighter,secondFighter);
  
  arena.append(healthIndicators, fighters);
  return arena;
}



function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, HealthPosition.left);
  const rightFighterIndicator = createHealthIndicator(rightFighter, HealthPosition.right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

enum HealthPosition {
  left = 'left-fighter-indicator',
  right = 'right-fighter-indicator'
}

function createHealthIndicator(fighter: IFighterDetails, position: HealthPosition ) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: position }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, FighterPosition.left);
  const secondFighterElement = createFighter(secondFighter, FighterPosition.right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

enum FighterPosition {
  left = 'arena___left-fighter',
  right = 'arena___right-fighter'
}

function createFighter(fighter :IFighterDetails, position:FighterPosition) {
  const imgElement = createFighterImage(fighter);
  /* const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter'; */
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${position}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
