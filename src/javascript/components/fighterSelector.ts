import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import {fighterService} from '../services/fightersService';
import { IFighter, IFighterDetails } from '../../interfaces/types';

const versusImg = '../../../resources/versus.png';

enum FighterPreviewPosition {
  left = 'fighter-preview___left',
  right = 'fighter-preview___right'
}

export function createFightersSelector() {
  let selectedFighters: IFighterDetails[] = [];
  return async <E>(event:E, fighterId:string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;

     //const firstFighter = playerOne ?? fighter;   //error during build
     //const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo; //error during build
     const firstFighter = playerOne === undefined || playerOne === null ? fighter: playerOne;
     const secondFighter = Boolean(playerOne) ? playerTwo === undefined || playerTwo === null ? fighter : playerTwo : playerTwo;
    
    selectedFighters = [firstFighter, secondFighter];

    if (Boolean(secondFighter)){
      renderSelectedFighters(selectedFighters);
    }
  };
}



const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId:string) : Promise<IFighterDetails> {
    if (fighterDetailsMap.has(fighterId)) {
      return fighterDetailsMap.get(fighterId)
    } else {
      fighterDetailsMap.set(fighterId,fighterService.getFighterDetails(fighterId))
      return fighterDetailsMap.get(fighterId)
    }
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]) :void {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, FighterPreviewPosition.left);
  const secondPreview = createFighterPreview(playerTwo, FighterPreviewPosition.right);
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview!.innerHTML = '';
  fightersPreview!.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters : IFighterDetails[]) {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () : void => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters:IFighterDetails[]) {
  renderArena(selectedFighters);
}
