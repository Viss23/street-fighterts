import { createElement } from '../helpers/domHelper';
import { IFighterDetails } from '../../interfaces/types';

enum FighterPreviewPosition {
  left = 'fighter-preview___left',
  right = 'fighter-preview___right'
}

export function createFighterPreview(fighter: IFighterDetails, position:FighterPreviewPosition) {
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root ${position}`,
  });

  const { name, health, attack, defense } = fighter;
  const fighterImage = createFighterImage(fighter);
  if (position=== "fighter-preview___right") {
    fighterImage.classList.add("flip-horizontal");
  }

  const fighterInfo = createElement({tagName: "div",className: "fighterInfo"});

  const fighterStats = createElement({tagName: "div",className: "fighterStats"});

  fighterStats.innerHTML = "Stats:";

  const fighterName = createElement({tagName: "p",className: "fighterName"});

  fighterName.innerHTML = `Name: ${name}`;

  const fighterHealth = createElement({tagName: "p",className: "fighterHp"});

  fighterHealth.innerHTML = `Hp: ${health}`;

  const fighterAttack = createElement({tagName: "p",className: "fighterAttack"});

  fighterAttack.innerHTML = `Attack: ${attack}`;

  const fighterDefense = createElement({tagName: "p",className: "fighterAttack"});

  fighterDefense.innerHTML = `Defense: ${defense}`;

  fighterInfo.append(
    fighterStats,
    fighterName,
    fighterHealth,
    fighterAttack,
    fighterDefense
  );
  fighterElement.append(fighterImage, fighterInfo);

  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
