import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { IFighter } from '../interfaces/types';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement | null = document.getElementById('root');
  static loadingElement: HTMLElement | null = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement!.style.visibility = 'visible';

      const fighters:IFighter[] = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      App.rootElement!.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement!.innerText = 'Failed to load data';
    } finally {
      App.loadingElement!.style.visibility = 'hidden';
    }
  }
}

export default App;
